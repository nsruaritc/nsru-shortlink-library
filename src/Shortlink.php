<?php
namespace Nsru\Shortlink;

use Exception;
use GuzzleHttp\Client;

class Shortlink
{
    private Client $client;
    private $accessToken = null;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://s.nsru.ac.th/api/',
            'verify' => false 
        ]);
    }

    public function setAccessToken($accessToken) {
        $this->accessToken = $accessToken;
    }

    public function getAccessToken($userId = null, $userToken = null) {
        try {
            if( \function_exists('env') ) {
                if( ! $userId )     $userId     = \env('NSRU_SHORTLINK_USER_ID',    'sample');
                if( ! $userToken )  $userToken  = \env('NSRU_SHORTLINK_USER_TOKEN', 'sample');
            }
            $response = $this->client->get("get/access-token/{$userId}/{$userToken}");
            if($response->getStatusCode() == 200) {
                $data = \json_decode($response->getBody()->getContents());
                if($data->status === "Success") {
                    $accessToken = $data->accessToken;
                    $this->setAccessToken($accessToken);
                    return $accessToken;
                } else throw new Exception('ไม่สามารถขอ Access Token ได้เนื่องจาก NSRU Shortlink ปฏิเสธการเข้าถึง');
            } else throw new Exception('ไม่สามารถขอ Access Token ได้เนื่องจากเชื่อมต่อกับ NSRU Shortlink API ไม่สำเร็จ');
        } catch(Exception $e) {
            return  false;
        }
    }

    public function getLink($originalUrl, $accessToken = null) {
        try {
            if( ! $accessToken ) $accessToken = $this->accessToken;
            $response = $this->client->get("generate/{$accessToken}?url={$originalUrl}");
            if($response->getStatusCode() == 200) {
                $url = $response->getBody()->getContents();
                if($url != "") {
                    return $url;
                } else throw new Exception('ไม่สามารถสร้าง Shortlink ได้เนื่องจาก Shortlink คืนค่าว่างกลับมา');
            } else throw new Exception('ไม่สามารถสร้าง Shortlink ได้เนื่องจากเชื่อมต่อกับ NSRU Shortlink API ไม่สำเร็จ');
        } catch(Exception $e) {
            return  false;
        }
    }

    public static function now($originalUrl) {
        $newShortLink = new Shortlink();
        $newShortLink->getAccessToken();
        return $newShortLink->getLink($originalUrl);
    }
}
